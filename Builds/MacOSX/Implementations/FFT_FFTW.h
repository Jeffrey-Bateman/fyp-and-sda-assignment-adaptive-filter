//
//  FFT_FFTW.h
//  Tevolution
//
//  Created by tj3-mitchell on 07/12/2013.
//
//

#ifndef H_FFT_FFTW
#define H_FFT_FFTW

#include "../FastFourierTransform.h"
#include "fftw3.h"

/**
 A simple class for wrapping the FFTW fft functions
 */
class FFT_FFTW : public FastFourierTransform
{
public:
    /**
     Constructor reciving the log2 of the fft size for setup. This also generates a window
     */
    FFT_FFTW(FFTSize sizeLog2_, FFTWindow windowType = Hanning);
    
    /**
     Destructor
     */
    ~FFT_FFTW();
    
    //FFT
    void transformMag(const float* const audio, float* const spectrumMag);
    
private:
    double* window;          //buffer for the window
    double* windowedAudio;   //buffer for the windowed audio
    float windowFactor; // window factor is sum of the window samples divided by the FFT size
    float oneOverWindowFactor;
    
    fftw_complex *fftOut;
    fftw_plan plan;
    
    //JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(FFT_FFTW);
};

#endif //H_FFT_FFTW
