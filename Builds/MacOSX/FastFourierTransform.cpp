/*
 *  FastFourierTransform.cpp
 *  ces
 *
 */

#include "FastFourierTransform.h"
#include <cmath>
#include <iostream>

FastFourierTransform::FastFourierTransform(FFTSize sizeLog2_, FFTWindow windowType) 
:   sizeLog2(sizeLog2_), size(1L << sizeLog2), halfSize(1L << sizeLog2-1), oneOverSize(1.f/size)
{        
    std::cout << "FFTSetup: fftSizeLog2=" << sizeLog2
        << " fftSize= "<< size << " fftSizeHalved="
        << halfSize << "oneOverfftSize=" << oneOverSize << "\n";


}

FastFourierTransform::~FastFourierTransform()
{

}

