/*
 *  FFT_vDSP.cpp
 *  ces
 *
 */

#include "FFT_vDSP.h"
#include <cmath>

FFT_vDSP::FFT_vDSP(FFTSize sizeLog2_, FFTWindow windowType) :
    FastFourierTransform (sizeLog2_, windowType)
{        
    window = new float[size];
    windowedAudio = new float[size];
    fftvDSP = vDSP_create_fftsetup (sizeLog2, 0);
    
    //render the window
    switch (windowType) 
    {
        case Hanning:   vDSP_hann_window(window, size, 0);      break;
        case Hamming:   vDSP_hamm_window(window, size, 0);      break;
        case Blackman:  vDSP_blkman_window(window, size, 0);    break;
    }
    //calculate the window factor
    vDSP_sve(window, 1, &windowFactor, size);
    windowFactor *= 1.f/size;
    oneOverWindowFactor = 1.f/windowFactor;
}

FFT_vDSP::~FFT_vDSP()
{
    vDSP_destroy_fftsetup(fftvDSP);
    delete[] windowedAudio;
    delete[] window;
}

void FFT_vDSP::transformMag(const float* const audio, float* const spectrumMag)
{
    //apply window
    vDSP_vmul(audio, 1, window, 1, windowedAudio, 1, size);
    
    //create 
    DSPSplitComplex fftBufferSplit;
    fftBufferSplit.realp = spectrumMag;
    fftBufferSplit.imagp = fftBufferSplit.realp + halfSize;
    
    vDSP_ctoz ((DSPComplex *) windowedAudio, 2, &fftBufferSplit, 1, halfSize);
    vDSP_fft_zrip (fftvDSP, &fftBufferSplit, 1, sizeLog2, FFT_FORWARD);
    
    //convert to magnitudes squared
    vDSP_zvmags(&fftBufferSplit, 1, spectrumMag, 1, halfSize+1);
    //square root (This cast seems to work ok)
    int halfSizei = halfSize;
    vvsqrtf(spectrumMag,spectrumMag, &halfSizei);
    
    vDSP_vsmul(spectrumMag, 1, &oneOverSize, spectrumMag, 1, halfSize+1);
    vDSP_vsmul(spectrumMag, 1, &oneOverWindowFactor, spectrumMag, 1, halfSize+1);
}

