//
//  FFT_FFTW.cpp
//  Tevolution
//
//  Created by tj3-mitchell on 07/12/2013.
//
//

#include "FFT_FFTW.h"
#include <cmath>
#include <iostream>

FFT_FFTW::FFT_FFTW(FFTSize sizeLog2_, FFTWindow windowType) :
FastFourierTransform (sizeLog2_, windowType),
windowFactor (0.0)
{
    window = new double[size];
    windowedAudio = new double[size];
    
    double twoPi = 2.0 * std::acos(-1);
    for(int i = 0; i < size; i++)
	{
		// Hann window equation
		window[i] = (1.0 - cos((double)i * (oneOverSize - 1) * twoPi));
		windowFactor += window[i];
	}
    windowFactor *= 1.f/size;
    oneOverWindowFactor = 1.f/windowFactor;
    
    fftOut = (fftw_complex*) fftw_malloc (sizeof(fftw_complex) * size);
    plan = fftw_plan_dft_r2c_1d ((int)size, windowedAudio, fftOut, FFTW_PATIENT);

}

FFT_FFTW::~FFT_FFTW()
{
    fftw_destroy_plan (plan);
    fftw_free (fftOut);
    
    delete[] windowedAudio;
    delete[] window;
}

void FFT_FFTW::transformMag(const float* const audio, float* const spectrumMag)
{
    
//    //apply window
    
    for(int i = 0; i < size; i++)
        windowedAudio[i] = audio[i] * window[i];
    
    //run fft
    fftw_execute (plan);
    
    for(int i = 0; i < halfSize; i++)
    {
        const float rawMagnitude = hypotf (fftOut[i][0], fftOut[i][1]);
        const float magnitudeForFFTSize = rawMagnitude * oneOverSize;
        spectrumMag[i] = magnitudeForFFTSize * oneOverWindowFactor;
    }
        
}

