/*
 *  FastFourierTransform.h
 *  ces
 */

#ifndef H_FASTFOURIERTRANSORM
#define H_FASTFOURIERTRANSORM

//#include "../../../_TevolutionDemo/JuceLibraryCode/JuceHeader.h"

/**
 A simple base class for FFTs
 */
class FastFourierTransform 
{
public:
    /**
     Enumeration for specifying the FFT size in the constructor.
     */
    enum FFTSize
    {
        Fft32 = 5,
        Fft64,
        Fft128,
        Fft256,
        Fft512,
        Fft1024,
        Fft2048,
        Fft4096,
        Fft8192,
        Fft16384,
        Fft32768
    };
    
    /**
     Enumerations for the window type
     */
    enum FFTWindow
    {
        Hanning,
        Hamming,
        Blackman
    };
    
    /**
     Constructor reciving the log2 of the fft size for setup. This also generates a window
     */
    FastFourierTransform(FFTSize sizeLog2_, FFTWindow windowType = Hanning);
    
    /**
     Destructor
     */
    virtual ~FastFourierTransform();
    
    /**
     Returns the FFT sizeLog2
     */
    inline const long getSizeLog2() const {return size;}
    
    /**
     Returns the FFT size
     */
    inline const long getSize() const {return size;}
    
    /**
     Returns half the FFT size
     */
    inline const long getHalfSize() const {return halfSize;}
    
    /**
     takes the audio performs a real to complex fft and then returns
     the spectrum magnatide. 
     
     The audio and spectrumMag buffers should be 
     the correct size specified in the constructor as the audio buffer, 
     even though the magnutides will only occupy elements 0 - halfSize+1 of 
     spectrumMag.
     */
    virtual void transformMag(const float* const audio, float* const spectrumMag) = 0;
    
protected:
    const long sizeLog2;
    const long size;
    const long halfSize;
    const float oneOverSize;
    
    
    //JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(FastFourierTransform);
};

#endif //H_FASTFOURIERTRANSORMER