/*
 *  FFT_vDSP.h
 *  ces
 */

#ifndef H_FFT_VDSP
#define H_FFT_VDSP

#include <Accelerate/Accelerate.h>
//#include "../../../../_TevolutionDemo/JuceLibraryCode/JuceHeader.h"
#include "../FastFourierTransform.h"

/**
 A simple class for wrapping the vDSP fft functions
 */
class FFT_vDSP : public FastFourierTransform
{
public:
    /**
     Constructor reciving the log2 of the fft size for setup. This also generates a window
     */
    FFT_vDSP(FFTSize sizeLog2_, FFTWindow windowType = Hanning);
    
    /**
     Destructor
     */
    ~FFT_vDSP();
    
    //FFT
    void transformMag(const float* const audio, float* const spectrumMag) override;
    
private:
    FFTSetup fftvDSP;
    
    
    //window - these should be in class
    float* window;          //buffer for the window
    float* windowedAudio;   //buffer for the windowed audio
    float windowFactor; // window factor is sum of the window samples divided by the FFT size
    float oneOverWindowFactor;

    
    //JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(FFT_vDSP);
};

#endif //H_FFT_VDSP